FROM ubuntu:bionic
LABEL maintainer="Filippo Rigotto <rigottofilippo@altervista.org>"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update \
&& apt-get -qqy install \
    libboost-all-dev \
    libeigen3-dev \
    libomp-dev \
    make \
    cmake \
    python3-pip\
    g++ \
    vim \
    rsync \
&& rm -rf /var/lib/apt/lists/*

RUN pip3 install virtualenv numpy scipy scikit-learn transforms3d h5py plyfile

ENTRYPOINT /bin/bash
