# docker-boost-eigen

![Boost](http://www.boost.org/doc/libs/1_59_0/boost.png)

![Boost 1.65.0](https://img.shields.io/badge/boost-1.65.0-brightgreen.svg)
![License MIT](https://img.shields.io/badge/license-MIT-blue.svg)
![Docker Registry: r/boost](https://img.shields.io/badge/docker-rig8f\/boost--eigen-blue.svg)

## Usage

```bash
sudo docker run -it --name my-boost-dev rig8f/boost-eigen
```
